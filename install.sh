#!/usr/bin/env bash

if [[ "$1" == "local" ]]; then
    mkdir -p ~/.explain-cron/lib ~/.explain-cron/bin
    install -m 755 ./lib/explain_cron.py ~/.explain-cron/lib/.
    install -m 755 ./bin/explain-cron ~/.explain-cron/bin/.
elif [[ -z "$1" ]]; then
    mkdir -p ~/.explain-cron/lib ~/.explain-cron/bin
    wget -O https://raw.githubusercontent.com/sansaid/explain-cron/v0.1.0/lib/explain_cron.py ~/.explain-cron/lib/explain_cron.py
    wget -O https://raw.githubusercontent.com/sansaid/explain-cron/v0.1.0/bin/explain-cron ~/.explain-cron/bin/explain-cron
else
    "Unrecognised argument(s) $@"
    exit 1
fi