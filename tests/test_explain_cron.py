import sys
import os

THIS_DIR = os.path.dirname(__file__)
sys.path.append(os.path.join(THIS_DIR, "../lib"))

from explain_cron import *
from unittest import TestCase

class TestAll(TestCase):
    def test_is_valid_token(self):
        self.assertTrue(All.is_valid_token(val="*"))
        self.assertFalse(All.is_valid_token(val="*/5"))
        self.assertFalse(All.is_valid_token(val=""))
        self.assertFalse(All.is_valid_token(val="5-10"))
        self.assertFalse(All.is_valid_token(val="*5-10"))
        self.assertFalse(All.is_valid_token(val="6"))

    def test_parse(self):
        self.assertListEqual(All("*", (0,23)).parse(), [*range(0, 24)])
        self.assertListEqual(All("*", (1, 5)).parse(), [*range(1, 6)])

        with self.assertRaises(ValueError):
            All("*/", (0, 12)).parse()

        with self.assertRaises(ValueError):
            All("*", (12, 2)).parse()

class TestAny(TestCase):
    def test_is_valid_token(self):
        self.assertTrue(Any.is_valid_token(val="?"))
        self.assertFalse(Any.is_valid_token(val="?/5"))
        self.assertFalse(Any.is_valid_token(val=""))
        self.assertFalse(Any.is_valid_token(val="5-10"))
        self.assertFalse(Any.is_valid_token(val="?5-10"))
        self.assertFalse(Any.is_valid_token(val="6"))

    def test_parse(self):
        self.assertListEqual(Any("?", (0,23)).parse(), [*range(0, 24)])
        self.assertListEqual(Any("?", (1, 5)).parse(), [*range(1, 6)])

        with self.assertRaises(ValueError):
            All("?/", (0, 12)).parse()

        with self.assertRaises(ValueError):
            All("?", (12, 2)).parse()


class TestNum(TestCase):
    def test_is_valid_token(self):
        # Testing within range cases
        self.assertTrue(Num.is_valid_token(val="6", range=(0,7)))
        self.assertTrue(Num.is_valid_token(val="67", range=(50, 80)))

        # Testing out of range cases
        self.assertFalse(Num.is_valid_token(val="67", range=(10, 20)))
        self.assertFalse(Num.is_valid_token(val="7", range=(10, 20)))

        # Testing invalid values cases
        self.assertFalse(Num.is_valid_token(val="?/5", range=(0, 10)))
        self.assertFalse(Num.is_valid_token(val="", range=(0,10)))
        self.assertFalse(Num.is_valid_token(val="5-10", range=(0,20)))
        self.assertFalse(Num.is_valid_token(val="*", range=(2,10)))
        self.assertFalse(Num.is_valid_token(val="?", range=(3,10)))

        # Testing invalid range case
        self.assertFalse(Num.is_valid_token(val="15", range=(20, 10)))

    def test_parse(self):
        self.assertListEqual(Num("7", (0,23)).parse(), [7])
        self.assertListEqual(Num("8", (0, 10)).parse(), [8])

        with self.assertRaises(ValueError):
            Num("?/", (0, 12)).parse()

        with self.assertRaises(ValueError):
            Num("?", (12, 2)).parse()


class TestNumRange(TestCase):
    def test_is_valid_token(self):
        # Testing within range cases
        self.assertTrue(NumRange.is_valid_token(val="2-3", range=(0,7)))
        self.assertTrue(NumRange.is_valid_token(val="67-70", range=(50, 80)))
        self.assertTrue(NumRange.is_valid_token(val="67-80", range=(50, 80)))
        self.assertTrue(NumRange.is_valid_token(val="50-70", range=(50, 80)))
        self.assertTrue(NumRange.is_valid_token(val="50-80", range=(50, 80)))

        # Testing out of range cases
        self.assertFalse(NumRange.is_valid_token(val="15-30", range=(10, 20)))
        self.assertFalse(NumRange.is_valid_token(val="5-15", range=(10, 20)))
        self.assertFalse(NumRange.is_valid_token(val="5-30", range=(10, 20)))

        # Testing invalid values cases
        self.assertFalse(NumRange.is_valid_token(val="?/5", range=(0, 10)))
        self.assertFalse(NumRange.is_valid_token(val="", range=(0,10)))
        self.assertFalse(NumRange.is_valid_token(val="10", range=(0,20)))
        self.assertFalse(NumRange.is_valid_token(val="*", range=(2,10)))
        self.assertFalse(NumRange.is_valid_token(val="?", range=(3,10)))

        # Testing invalid range case
        self.assertFalse(NumRange.is_valid_token(val="15-30", range=(20, 10)))

    def test_parse(self):
        self.assertListEqual(NumRange("10-20", (0,23)).parse(), [*range(10,21)])
        self.assertListEqual(NumRange("8-10", (0, 10)).parse(), [*range(8,11)])

        with self.assertRaises(ValueError):
            NumRange("?/", (0, 12)).parse()

        with self.assertRaises(ValueError):
            NumRange("?", (12, 2)).parse()


class TestIncrement(TestCase):
    def test_is_valid_token(self):
        # Testing within range cases
        self.assertTrue(Increment.is_valid_token(val="2/5", range=(0,7)))
        self.assertTrue(Increment.is_valid_token(val="67-70/10", range=(50, 80)))
        self.assertTrue(Increment.is_valid_token(val="*/5", range=(50, 80)))
        self.assertTrue(Increment.is_valid_token(val="60/10", range=(50, 80)))
        self.assertTrue(Increment.is_valid_token(val="*/10", range=(50, 80)))

        # Testing out of range cases
        self.assertFalse(Increment.is_valid_token(val="15-30/5", range=(10, 20)))
        self.assertFalse(Increment.is_valid_token(val="5-15/10", range=(10, 20)))
        self.assertFalse(Increment.is_valid_token(val="5-30/5", range=(10, 20)))

        # Testing invalid values cases
        self.assertFalse(Increment.is_valid_token(val="?/5", range=(0, 10)))
        self.assertFalse(Increment.is_valid_token(val="/10", range=(0, 10)))
        self.assertFalse(Increment.is_valid_token(val="-9/10", range=(0, 10)))
        self.assertFalse(Increment.is_valid_token(val="9-/10", range=(0, 10)))
        self.assertFalse(Increment.is_valid_token(val="", range=(0,10)))
        self.assertFalse(Increment.is_valid_token(val="10", range=(0,20)))
        self.assertFalse(Increment.is_valid_token(val="*", range=(2,10)))
        self.assertFalse(Increment.is_valid_token(val="?", range=(3,10)))

        # Testing invalid range case
        self.assertFalse(Increment.is_valid_token(val="15-30/5", range=(20, 10)))

    def test_parse(self):
        self.assertListEqual(Increment("*/5", (0,10)).parse(), [*range(0,11,5)])
        self.assertListEqual(Increment("10/5", (0,23)).parse(), [*range(10,24,5)])
        self.assertListEqual(Increment("10-20/5", (0,23)).parse(), [*range(10,21,5)])
        self.assertListEqual(Increment("8-10/2", (0, 10)).parse(), [*range(8,11,2)])

        with self.assertRaises(ValueError):
            Increment("?/", (0, 12)).parse()

        with self.assertRaises(ValueError):
            Increment("?", (12, 2)).parse()


class TestCronMinutes(TestCase):
    def test_minutes_triggers(self):
        # Test valid cases
        self.assertListEqual(CronMinute("*").triggers, [*range(0,60)])
        self.assertListEqual(CronMinute("20").triggers, [20])
        self.assertListEqual(CronMinute("10-20").triggers, [*range(10,21)])
        self.assertListEqual(CronMinute("5/3").triggers, [*range(5,60,3)])
        self.assertListEqual(CronMinute("7-10/3").triggers, [7, 10])
        self.assertListEqual(CronMinute("6,10,11").triggers, [6, 10, 11])
        self.assertListEqual(CronMinute("15,10-20/5").triggers, [10, 15, 20])
        self.assertListEqual(CronMinute("15,10-20/5,*").triggers, [*range(0,60)])
    
        # Test invalid cases
        with self.assertRaises(ValueError):
            CronMinute("?").triggers
        
        with self.assertRaises(ValueError):
            CronMinute("abc").triggers

        with self.assertRaises(ValueError):
            CronMinute("78").triggers
        
        with self.assertRaises(ValueError):
            CronMinute("10-70").triggers


class TestCronHour(TestCase):
    def test_hour_triggers(self):
        # Test valid cases
        self.assertListEqual(CronHour("*").triggers, [*range(0,24)])
        self.assertListEqual(CronHour("20").triggers, [20])
        self.assertListEqual(CronHour("10-20").triggers, [*range(10,21)])
        self.assertListEqual(CronHour("5/3").triggers, [*range(5,24,3)])
        self.assertListEqual(CronHour("7-10/3").triggers, [7, 10])
        self.assertListEqual(CronHour("6,10,11").triggers, [6, 10, 11])
        self.assertListEqual(CronHour("15,10-20/5").triggers, [10, 15, 20])
        self.assertListEqual(CronHour("15,10-20/5,*").triggers, [*range(0,24)])
    
        # Test invalid cases
        with self.assertRaises(ValueError):
            CronHour("?").triggers
        
        with self.assertRaises(ValueError):
            CronHour("abc").triggers

        with self.assertRaises(ValueError):
            CronHour("78").triggers
        
        with self.assertRaises(ValueError):
            CronHour("10-70").triggers


class TestCronMonthDay(TestCase):
    def test_month_day_triggers(self):
        # Test valid cases
        self.assertListEqual(CronMonthDay("*").triggers, [*range(1,32)])
        self.assertListEqual(CronMonthDay("?").triggers, [*range(1,32)])
        self.assertListEqual(CronMonthDay("20").triggers, [20])
        self.assertListEqual(CronMonthDay("10-20").triggers, [*range(10,21)])
        self.assertListEqual(CronMonthDay("5/3").triggers, [*range(5,32,3)])
        self.assertListEqual(CronMonthDay("7-10/3").triggers, [7, 10])
        self.assertListEqual(CronMonthDay("6,10,11").triggers, [6, 10, 11])
        self.assertListEqual(CronMonthDay("15,10-20/5").triggers, [10, 15, 20])
        self.assertListEqual(CronMonthDay("15,10-20/5,*").triggers, [*range(1,32)])
    
        # Test invalid cases
        with self.assertRaises(ValueError):
            CronMonthDay("abc").triggers

        with self.assertRaises(ValueError):
            CronMonthDay("78").triggers
        
        with self.assertRaises(ValueError):
            CronMonthDay("10-70").triggers

        with self.assertRaises(ValueError):
            CronMonthDay("0-30").triggers   


class TestCronMonth(TestCase):
    def test_month_day_triggers(self):
        # Test valid cases
        self.assertListEqual(CronMonth("*").triggers, [*range(1,13)])
        self.assertListEqual(CronMonth("10").triggers, [10])
        self.assertListEqual(CronMonth("2-10").triggers, [*range(2,11)])
        self.assertListEqual(CronMonth("5/3").triggers, [*range(5,13,3)])
        self.assertListEqual(CronMonth("7-10/3").triggers, [7, 10])
        self.assertListEqual(CronMonth("6,10,11").triggers, [6, 10, 11])
        self.assertListEqual(CronMonth("1,5-10/2").triggers, [1, 5, 7, 9])
        self.assertListEqual(CronMonth("1,5-10/2,*").triggers, [*range(1,13)])
    
        # Test invalid cases
        with self.assertRaises(ValueError):
            CronMonth("?").triggers

        with self.assertRaises(ValueError):
            CronMonth("abc").triggers

        with self.assertRaises(ValueError):
            CronMonth("78").triggers
        
        with self.assertRaises(ValueError):
            CronMonth("10-70").triggers

        with self.assertRaises(ValueError):
            CronMonth("0-11").triggers


class TestCronWeekDay(TestCase):
    def test_week_day_triggers(self):
        # Test valid cases
        self.assertListEqual(CronWeekDay("*").triggers, [*range(1,8)])
        self.assertListEqual(CronWeekDay("?").triggers, [*range(1,8)])
        self.assertListEqual(CronWeekDay("5").triggers, [5])
        self.assertListEqual(CronWeekDay("2-5").triggers, [*range(2,6)])
        self.assertListEqual(CronWeekDay("1/2").triggers, [*range(1,8,2)])
        self.assertListEqual(CronWeekDay("1-6/2").triggers, [*range(1,7,2)])
        self.assertListEqual(CronWeekDay("1,3,4").triggers, [1, 3, 4])
        self.assertListEqual(CronWeekDay("1,2-6/2").triggers, [1, 2, 4, 6])
        self.assertListEqual(CronWeekDay("1,2-6/2,*").triggers, [*range(1,8)])
    
        # Test invalid cases
        with self.assertRaises(ValueError):
            CronWeekDay("abc").triggers

        with self.assertRaises(ValueError):
            CronWeekDay("78").triggers
        
        with self.assertRaises(ValueError):
            CronWeekDay("2-70").triggers

        with self.assertRaises(ValueError):
            CronWeekDay("0-6").triggers