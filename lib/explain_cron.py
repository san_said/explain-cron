from abc import ABC, abstractmethod
from functools import reduce
import argparse
import re

class CronToken(ABC):
    """
    CronToken

    Abstract class used to define CronToken implementations.

    A CronToken is used to represent an individual token in a cron expression. Example:

      --------------------------------------------- Anything in brackets can be represented as a cron token
      |   ___|___   |   |   |   |   |
    (15),(16-20/5),(*) (*) (*) (?) (7) /bin/cmd 

    Logically, a cron token is anything that can be used in isolation in a cron expression to denote a sequence of numbers.
    """
    def __init__(self, val, valid_range):
        self.valid_range = valid_range
        self.val = val
        self.is_valid_range()
    
    def is_valid_range(self):
        if len(self.valid_range) != 2:
            raise ValueError(f"Range not recognised: {self.valid_range}")
        
        (_min, _max) = self.valid_range

        if _min > _max:
            raise ValueError(f"Min in range should not be greater than max: {self.valid_range}") 

        (self._min, self._max) = (_min, _max)

    @property
    @abstractmethod
    def regex(self):
        raise NotImplementedError("CronToken subclass must implement a regex attribute")

    @abstractmethod
    def parse(self):
        raise NotImplementedError("CronToken subclass must implement a parse method")

    @abstractmethod
    def is_valid_token(self, **kwargs):
        raise NotImplementedError("CronToken subclass must implement a is_valid_token method")


class All(CronToken):
    """
    All < CronToken

    Class used to represent the * cron token
    """

    regex = r"^\*$"

    def __init__(self, val, valid_range):
        super().__init__(val, valid_range)
    
    @staticmethod
    def is_valid_token(**kwargs):
        val = kwargs['val']

        if not re.match(All.regex, val):
            return False

        return True

    def parse(self):
        if not All.is_valid_token(val=self.val):
            raise ValueError("All value not recognised")

        (_min, _max) = self.valid_range

        return [*range(_min, _max+1)]


class Any(CronToken):
    """
    Any < CronToken

    Class used to represent the ? cron token
    """
    regex = r"^\?$"

    def __init__(self, val, valid_range):
        super().__init__(val, valid_range)
    
    @staticmethod
    def is_valid_token(**kwargs):
        val = kwargs['val']

        if not re.match(Any.regex, val):
            return False

        return True

    def parse(self):
        if not Any.is_valid_token(val=self.val):
            raise ValueError("Any value not recognised")

        (_min, _max) = self.valid_range

        return [*range(_min, _max+1)]


class Num(CronToken):
    """
    Num < CronToken

    Class used to represent an individual number as a cron token
    """
    regex = r"^(\d+)$"

    def __init__(self, val, valid_range):
        super().__init__(val, valid_range)
    
    @staticmethod
    def is_valid_token(**kwargs):
        val = kwargs['val']
        _range = kwargs['range']
        
        (_min, _max) = _range

        if not re.match(Num.regex, val):
            return False
        
        int_val = int(val)

        if int_val > _max or int_val < _min:
            return False

        return True
    
    @staticmethod
    def get_num(val):
        return list(map(int, re.match(Num.regex, val).groups(0)))

    def parse(self):
        if not Num.is_valid_token(val=self.val, range=self.valid_range):
            raise ValueError("Num value not recognised")
        
        return Num.get_num(self.val)


class NumRange(CronToken):
    """
    NumRange < CronToken

    Class used to represent a number range as a cron token
    """
    regex = r"^(\d+)-(\d+)$"

    def __init__(self, val, valid_range):
        super().__init__(val, valid_range)

    @staticmethod
    def is_valid_token(**kwargs):
        val = kwargs['val']
        _range = kwargs['range']

        matches = re.match(NumRange.regex, val)

        if not matches:
            return False
        
        groups = matches.groups(0)

        if len(groups) != 2:
            return False
        
        (lower_lim, upper_lim) = list(map(int, groups))
        (_min, _max) = _range

        lower_out_of_bounds = lower_lim < _min or lower_lim > _max
        upper_out_of_bounds = upper_lim < _min or upper_lim > _max

        if lower_out_of_bounds or upper_out_of_bounds:
            return False
        
        return True
    
    @staticmethod
    def get_range(val):
        return list(map(int, re.match(NumRange.regex, val).groups(0)))

    def parse(self):
        if not NumRange.is_valid_token(val=self.val, range=self.valid_range):
            raise ValueError("NumRange value not recognised")

        (lower_lim, upper_lim) = NumRange.get_range(self.val)

        return [*range(lower_lim, upper_lim+1)]


class Increment(CronToken):
    """
    Increment < CronToken

    Class used to represent an increment as a cron token. The Incremement cron token can be made of an All, Num, NumRange cron tokens.
    """
    regex = r"^(?:(\d+(?:-\d+)?|\*))(?:\/(\d+))$"

    def __init__(self, val, valid_range):
        super().__init__(val, valid_range)

    @staticmethod
    def is_valid_token(**kwargs):
        val = kwargs['val']
        _range = kwargs['range']
        
        matches = re.match(Increment.regex, val)
        allowed_condition_types = [All, Num, NumRange]

        if not matches:
            return False

        groups = matches.groups(0)

        if len(groups) != 2:
            return False
        
        (condition, increment) = groups

        condition_type_matches = 0

        for condition_type in allowed_condition_types:
            condition_type_matches += 1 if condition_type.is_valid_token(val=condition, range=_range) else 0

        if condition_type_matches != 1:
            return False
        
        return True

    def parse(self):
        if not Increment.is_valid_token(val=self.val, range=self.valid_range):
            raise ValueError("Increment value not recognised")

        (lower_lim, upper_lim) = self.valid_range

        (condition, increment) = re.match(Increment.regex, self.val).groups(0)
        
        if Num.is_valid_token(val=condition, range=self.valid_range):
            [lower_lim] = Num.get_num(condition) 
        elif NumRange.is_valid_token(val=condition, range=self.valid_range):
            (lower_lim, upper_lim) = NumRange.get_range(condition)

        return [*range(lower_lim, upper_lim+1, int(increment))]


class CronUnit:
    """
    CronUnit

    Base class to represent a unit of time in a cron expression. This is needed as each unit of time will accept a limited list of cron tokens.

          ----------------------------------- Anything in brackets can be represented as a cron unit
     _____|______   |   |   |   | 
    (15,16-20/5,*) (*) (*) (?) (7) /bin/cmd 

    Logically, a cron unit is a unit of time in a cron expression.

    To create a new CronUnit, you must inherit from this base class and implement the allowed_tokens and valid_range class properties. Everything else must not be overriden.

    * allowed_tokens = [token: CronToken, ...] - list of CronTokens supported by the unit
    * valid_range = (min: int, max: int) - tuple of length two that represent a range of numbers supported by the unit
    """
    @property
    @staticmethod
    def allowed_tokens(self):
        raise NotImplementedError("CronUnit subclass must implement allowed_tokens attribute")

    @property
    @staticmethod
    def valid_range(self):
        raise NotImplementedError("CronUnit subclass must implement valid_range attribute")

    def __init__(self, unit):
        self.unit = unit
        self.vals = unit.split(",")

        self._initialise_tokens(self.vals)
        self.parse()
    
    def _initialise_tokens(self, vals):
        tokens = []

        for val in vals:
            for token in self.allowed_tokens:
                if token.is_valid_token(val=val, range=self.valid_range):
                    tokens.append(token(val, self.valid_range))

        if len(tokens) != len(vals):
            raise ValueError("Invalid tokens found")
        
        self.tokens = tokens
    
    def parse(self):
        if not hasattr(self, 'triggers'):
            triggers = set([])

            if not hasattr(self, 'tokens'):
                self._initialise_tokens()
            
            for token in self.tokens:
                triggers = triggers.union(token.parse())
        
        self.triggers = sorted(triggers)

        return self.triggers


class CronMinute(CronUnit):
    allowed_tokens = [All, Increment, Num, NumRange]
    valid_range = (0, 59)

    def __init__(self, unit):
        super().__init__(unit)


class CronHour(CronUnit):
    allowed_tokens = [All, Increment, Num, NumRange]
    valid_range = (0, 23)

    def __init__(self, unit):
        super().__init__(unit)


class CronMonthDay(CronUnit):
    allowed_tokens = [All, Any, Increment, Num, NumRange]
    valid_range = (1, 31)

    def __init__(self, unit):
        super().__init__(unit)


class CronMonth(CronUnit):
    allowed_tokens = [All, Increment, Num, NumRange]
    valid_range = (1, 12)

    def __init__(self, unit):
        super().__init__(unit)


class CronWeekDay(CronUnit):
    allowed_tokens = [All, Any, Increment, Num, NumRange]
    valid_range = (1, 7)

    def __init__(self, unit):
        super().__init__(unit)


if __name__ == "__main__":
    ec = argparse.ArgumentParser(
        prog="explain-cron",
        description="Human friendly cron expression explainer"
    )

    ec.add_argument(
        'expression',
        nargs=1,
        help="The cron expression you want to explain to the human"
    )

    ec_args = ec.parse_args()
    expr = ec_args.expression[0]

    cron_units = expr.split(" ")

    if len(cron_units) != 6:
        raise ValueError("Your cron expression is not valid. Please make sure it is in the following format: "
                        "\"<minutes> <hours> <day of month> <month> <day of week> <command>\"")

    # cross_unit_mapper is used to determine the label of each unit in the cron expression as well as how it will
    # be parsed. Each mapper is a tuple and the positioning of each mapper should match the index of the unit in the
    # cron expression it corresponds to. The tuple should contain: the label name (0th index - used in the output 
    # message), the CronUnit object representing the the unit (1st index - the cron expression unit will be passed 
    # into this object as a string for parsing). 
    cron_unit_mapper = [
        ("minute", CronMinute),
        ("hour", CronHour),
        ("day of month", CronMonthDay),
        ("month", CronMonth),
        ("day of week", CronWeekDay)
    ]

    output = ""

    for i, mapper in enumerate(cron_unit_mapper):
        (label, cron_obj) = mapper
        triggers = cron_obj(cron_units[i]).triggers
        triggers_str = list(map(str, triggers))
        triggers_str = " ".join(triggers_str)

        output += "{:<12}\t{:<14}\n".format(label, triggers_str)

    output += "{:<12}\t{:<14}".format("command", cron_units[len(cron_units)-1])

    print(output)