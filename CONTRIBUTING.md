# Contributing

## Prerequisites

* Python 3.8+
* `pytest`

You can start contributing to this project by cloning this repo. The application does not require installation of any external Python packages to run, but in order to test, you need to have `pytest` installed.

## Testing

All tests must run cleanly for any PRs to be accepted in this repo. All tests must be stored in the `tests` directory. As this application is maintained as a single file (for now), there is only a single test file that currently exists.

Any new test files that need to be added should go in the `tests` directory and must add the `lib` folder to the system path at the start of the file (see [test_explain_cron.py](./tests/test_explain_cron.py)).

## Adding new features

Any additional Python files should go in `./lib`. `./install.sh` is responsible for performing the installation of the contents of `./lib` into a folder stored in the user's home directory. All installation should target a path in the user's home directory to limit the requirement of special privileges in order to install. The user is left to add the path manually.

## Branching rules

Any features must be done on feature branches that are based on the master branch. PRs should be targetted towards the master branch. Pushes to master **must not** be done directly.